#include "sorting-main.h"
/*
 * # Автор: Igor A. Shmakov
 * # Первая версия: 2018-02-17
 * # Версия: 0.3
 * # Лицензия: GPLv3
 */
/*
 * iteration_number = len(unordered_list)-1
 * for i in range(iteration_number):
 *   for j in range(iteration_number):
 *     if unordered_list[j] > unordered_list[j+1]:
 *       temp = unordered_list[j]
 *       unordered_list[j] = unordered_list[j+1]
 *       unordered_list[j+1] = temp
 */
void BubbleSort(int arraySort[], int arrayUnSort[], int size) {
  int temporary = 0;
  for (int i = 0; i < size; i++) {
    arraySort[i] = arrayUnSort[i];
  }

  for (int i = 0; i < size - 1; i++) {
    for (int j = 0; j < size - 1; j++) {
      if (arraySort[j] > arraySort[j+1]) {
	temporary = arraySort[j];
	arraySort[j] = arraySort[j+1];
	arraySort[j+1] = temporary;
      }
    }
  }
}
