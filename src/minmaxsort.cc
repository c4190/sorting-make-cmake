#include <iostream>
/*
 * # Автор: Igor A. Shmakov
 * # Первая версия: 2018-02-17
 * # Версия: 0.2
 * # Лицензия: GPLv3
 */

void MinMaxSort(int sort[], int unsort[], int size) {
  for (int i = 0; i < size; i++) {
    sort[i] = unsort[i];
  }
  int min = 0;
  int max = 0;
  int indexMin = 0;
  int indexMax = 0;
  int left = 0;
  int right = size;
  int temp = 0;

  while (size > 0) {
    min = sort[left];
    max = sort[left];
    //    std::cout << "min = " << min << " max = " << max << " size = " << size << std::endl;

    for (int i = left; i < right; i++) {
      if (min > sort[i]) {
		std::cout << sort[left] << " <l,i> " << sort[i] << std::endl;
		temp = sort[left];
		min = sort[i];
		sort[left] = min;
		sort[i] = temp;
		std::cout << sort[left] << " <l,i> " << sort[i] << std::endl;
		std::cout << "min = " << min << std::endl;
      }
      if (max < sort[i]) {
		std::cout << "max = " << max << " " << sort[right - 1] << " <r,i> " << sort[i] << std::endl;
		temp = sort[right - 1];
		max = sort[i];
		sort[right - 1] = max;
		sort[i] = temp;
		std::cout << sort[right - 1] << " <r,i> " << sort[i] << std::endl;
		std::cout << "max = " << max << std::endl;
      }
    }
    std::cout << "min = " << min << " max = " << max << " size = " << size << std::endl;
    /*
    temp = sort[left];
    sort[left] = min;
    sort[indexMin] = temp;

    std::cout << "indexMin = " << indexMin << " min = " << min << std::endl;

    if (max != sort[right - 1]) {
      temp = sort[right - 1];
      sort[right - 1] = max;
      sort[indexMax] = temp;
    }
    */
    //    std::cout << "left = " << left << " right = " << right << std::endl;
    //    std::cout << "indexMax = " << indexMax << " max = " << max << std::endl;

    //    std::cout << "sort[left] = " << sort[left] << " sort[right] = " << sort[right] << std::endl;
    left += 1;
    right -= 1;
    size -= 2;
    //    std::cout << "min = " << min << " max = " << max << " size = " << size << std::endl;
  }
}
  /*
  int stopSort = size;
  int min = 0;
  int max = 0;
  int temp = 0;
  int left = 0;
  int right = size;

  for (int i = 0; i < stopSort; i++) {
    min = sort[i];
    max = sort[i];
    // [34, 53, 23, 57, 84, 23]
    for (int j = left; j < right - 1; j++) {
      if (min > sort[j + 1]) {
	temp = min;
	min = sort[j + 1];
	sort[j + 1] = temp;
      }
      if (max < sort[j + 1]) {
	temp = max;
	max = sort[j + 1];
	sort[j + 1] = temp;
      }
    }
    sort[i] = min;
    sort[stopSort] = max;
    stopSort -= 1;
    left = i;
    right -= 1;
  }
  */
