#include "sorting-main.h"
/*
 * # Автор: Igor A. Shmakov
 * # Первая версия: 2018-02-17
 * # Версия: 0.5
 * # Лицензия: GPLv3
 */

#define SIZE 50

int main(int argc, char *argv[]) {

  srand(time(NULL));
#if 0
  int arraysize = SIZE;
  int sort[SIZE] = {0};
  int unsort[SIZE] = {0};
#endif
#if 1
  int arraysize;
  if (argc == 1) {
    std::cout << "На вход программы подаётся длинна генерируемого массива" << std::endl;
    return 1;
  }
  sscanf(argv[1], "%i", &arraysize);
  int *sort = new int[arraysize];
  int *unsort = new int[arraysize];
#endif

  // Создание несортированного массива:
  for (int i = 0; i < arraysize; i++) {
    unsort[i] = rand() % 100;
  }

  // Сортировка методом <<Пузырька>>
  BubbleSort(sort, unsort, arraysize);
  //MinMaxSort(sort, unsort, arraysize);

  // Вывод сортированного массива:
  for (int i = 0; i < arraysize; i++) {
    std::cout << unsort[i] << " " << sort[i] << std::endl;
  }

  return 0;
}
