#include <iostream>
#include <ctime>
#include <cstdio>
#include <cstdlib>

/*
 * # Автор: Igor A. Shmakov
 * # Первая версия: 2018-02-17
 * # Версия: 0.4
 * # Лицензия: GPLv3
 */

void BubbleSort(int *arraySort, int *arrayUnSort, int size);
void MinMaxSort(int *sort, int *unsort, int size);
