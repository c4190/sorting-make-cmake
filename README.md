# Запуск под GNU/Linux
Создайте директорию build-linux, перейдите в неё и запустите генерацию cmake с последующим запуском make:

```shell
~$ mkdir build-linux
~$ cmake ../
~$ make -j3
```

# Запуск под MS Windows
Для запуска вам потребуется MSYS2 и VS Code. Инструкция по установке: https://code.visualstudio.com/docs/cpp/config-mingw.


В установленном MSYS2 вам потребуется установить несколько пакетовЁ следующей командой:
```shell
~$ pacman -S mingw-w64-x86_64-gcc mingw-w64-x86_64-gdb mingw-w64-x86_64-cmake
```

Добавьте в переменную окружения PATH: C:\msys64\mingw64\bin.

Теперь можно открыть директорию с проектом в VS Code и выбрать в нижней части программы выбрать GCC 12. После чего можно собирать проект и запускать.
